alter table photos_status add notification_sent bit not null default 0;

update users set fake = 0 where fake is null;
update users set deleted = 0 where deleted is null;

alter table users modify fake bit not null default false;
alter table users modify deleted bit not null default false;