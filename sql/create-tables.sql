drop table photos;
create table photos (
  id integer auto_increment primary key, 
  user_id integer,
  up_votes integer,
  down_votes integer,
  photo_url varchar(256),
  reported integer default 0,
  created_at timestamp default current_timestamp
);

drop table users;
create table users (
  id integer auto_increment primary key,
  username varchar(100) unique,
  email varchar(120),
  google_reg_id varchar(120),
  apple_reg_id varchar(64),
  deleted bit default false,
  fake bit default false,
  created_at timestamp default current_timestamp
);

drop table photos_status;
create table photos_status (
  photo_id integer,
  user_id integer,
  status ENUM("like", "dislike", "unknown"),
  photo_owner_id integer,
  created_at timestamp default current_timestamp,
  PRIMARY KEY (photo_id, user_id)
);

drop table system_info;
create table system_info ( name varchar(30) , value varchar(100) );
