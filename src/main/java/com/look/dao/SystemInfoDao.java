package com.look.dao;

import com.look.model.Photo;
import com.look.model.Property;

import java.util.List;

public interface SystemInfoDao {

    List<Property> getSystemProperties();
}
