package com.look.dao;

import com.look.model.Photo;
import com.look.model.PendingPhoto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: ivan
 * Date: 11.09.14
 * Time: 22:01
 */
public interface PhotoDao {

    List<Photo> getPhotosList();

    Photo getPhoto(long id);

    void likePhoto(long id);

    void dislikePhoto(long id);

    int addPhoto(@Param("photo") Photo photo);

    List<Photo> getUserPhotos(@Param("user_id") long userId);

    List<Photo> getUserPendingPhotos(@Param("user_id") long userId);

    List<Photo> getPhotosForNotification();

    // Use ups and downs as parametrs to substract values and not use lock and not conflict with likeJob
    void resetPhotoNotificationStat(@Param("photo_id") long photoId, @Param("ups") int ups, @Param("downs") int downs);

    void pendPhotoForUser(@Param("photo_id") long photoId, @Param("user_id") long userId, @Param("photo_owner_id") long photoOwnerId);

    void updatePendingPhotoStatus(@Param("photo_id") long photoId, @Param("user_id") long userId, @Param("status") String status);

    void report(long id);

    String getPhotoStatus(@Param("photo_id") long photoId, @Param("user_id") long userId);

    List<PendingPhoto> getPendingPhotos();

    void updatePendingPhotoNotificationStatus(@Param("photo_id") long photoId, @Param("user_id") long userId);



}
