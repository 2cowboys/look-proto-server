package com.look.dao;

import com.look.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    void createUser(User user);

    User getUser(@Param("username") String username);

    User getUserById(@Param("user_id") long user_id);

    List<User> getListOfUsers();

    List<User> getListOfFakeUsers();

    void removeUser(@Param("username") String username);
}
