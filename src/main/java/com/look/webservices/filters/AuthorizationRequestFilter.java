package com.look.webservices.filters;

import com.look.webservices.resources.SystemInfoResource;
import com.restfb.FacebookClient;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.List;

@Provider
public class AuthorizationRequestFilter implements ContainerRequestFilter {

    @Inject
    private FacebookClient fbClient;

    @Override
    public void filter(ContainerRequestContext requestContext)
            throws IOException {
        List<PathSegment> pathSegments = requestContext.getUriInfo().getPathSegments();
        if (pathSegments.size() == 1 && "application.wadl".equalsIgnoreCase(pathSegments.get(0).getPath())
                || requestedResourceIs(requestContext, SystemInfoResource.class)) {
            return;
        }

        String accessToken = requestContext.getHeaderString("access-token");

        if (StringUtils.isBlank(accessToken)) {
            abort(requestContext);
            return;
        }

        FacebookClient.DebugTokenInfo result = fbClient.debugToken(accessToken);
        if (!result.isValid() && (!"fake-token".equals(accessToken))) {
            abort(requestContext);
        }
    }

    private boolean requestedResourceIs(ContainerRequestContext requestContext, Class<?> resourceClass) {
        for (Object resource : requestContext.getUriInfo().getMatchedResources()) {
            if (resourceClass.equals(resource.getClass())) {
                return true;
            }
        }
        return false;
    }

    private void abort(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response
                .status(Response.Status.UNAUTHORIZED)
                .entity("User cannot access the resource.")
                .build());
    }

    public FacebookClient getFbClient() {
        return fbClient;
    }

    public void setFbClient(FacebookClient fbClient) {
        this.fbClient = fbClient;
    }
}
