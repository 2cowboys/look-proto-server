package com.look.webservices.resources;

import com.look.dao.SystemInfoDao;
import com.look.model.Property;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("systeminfo")
@Component
public class SystemInfoResource {

    @Inject
    private SystemInfoDao systemInfo;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getIt() {
        StringBuilder builder = new StringBuilder();
        List<Property> systemProperties = systemInfo.getSystemProperties();
        for (Property systemProperty : systemProperties) {
            builder
                    .append(systemProperty.getName())
                    .append(" : ")
                    .append(systemProperty.getValue())
                    .append(";")
                    .append("\n");
        }
        return builder.toString();
    }

    public SystemInfoDao getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(SystemInfoDao systemInfo) {
        this.systemInfo = systemInfo;
    }
}
