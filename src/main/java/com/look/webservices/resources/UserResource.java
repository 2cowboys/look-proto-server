package com.look.webservices.resources;

import com.look.dao.PhotoDao;
import com.look.model.Photo;
import com.look.model.User;
import com.look.services.UserService;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

import org.slf4j.LoggerFactory;


@Path("user")
public class UserResource {

    private final Logger LOGGER = LoggerFactory.getLogger(UserResource.class);
    @Inject
    private UserService userService;
    @Inject
    private PhotoDao photoDao;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(User user) throws IOException {
        LOGGER.info("Create user " + user);
        userService.createUser(user);
        return Response.ok().build();
    }

    @GET
    @Path("{username}")
    public User getUser(@PathParam("username") String username) throws IOException {
        User user = userService.getUser(username);
        if (user == null) {
            throw new NotFoundException("No user with name: " + username);
        }
        return user;
    }

    @DELETE
    @Path("{username}")
    public Response deleteUser(@PathParam("username") String username) throws IOException {
        userService.removeUser(username);
        return Response.ok().build();
    }

    @GET
    @Path("{username}/photos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Photo> getUserPhotos(@PathParam("username") String username) {

        List<Photo> photos;
        User user = userService.getUser(username);
        if (user != null) {
            photos = photoDao.getUserPhotos(user.getId());
        } else {
            throw new NotFoundException("No user with name: " + username);
        }
        return photos;
    }

    @GET
    @Path("{username}/photos/pending")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Photo> getUserPendingPhotos(@PathParam("username") String username) {

        List<Photo> photos;
        User user = userService.getUser(username);
        if (user != null) {
            photos = photoDao.getUserPendingPhotos(user.getId());
        } else {
            throw new NotFoundException("No user with name: " + username);
        }
        return photos;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public PhotoDao getPhotoDao() {
        return photoDao;
    }

    public void setPhotoDao(PhotoDao photoDao) {
        this.photoDao = photoDao;
    }
}
