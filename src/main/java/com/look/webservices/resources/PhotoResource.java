package com.look.webservices.resources;

import com.look.dao.PhotoDao;
import com.look.model.Photo;
import com.look.model.PhotoStatus;
import com.look.model.User;
import com.look.model.dto.UsernameDto;
import com.look.services.NotificationService;
import com.look.services.PhotoService;
import com.look.services.UserService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

/**
 * @Author: ivan
 * Date: 11.09.14
 * Time: 20:15
 */
@Path("photo")
@Component
public class PhotoResource {

    @Inject
    private PhotoDao photoDao;
    @Inject
    private PhotoService photoService;
    @Inject
    private UserService userService;
    @Inject
    private NotificationService notificationService;

    public static final int CHUNK_SIZE = 1024;
    private final Logger LOGGER = Logger.getLogger(PhotoResource.class.getName());

    /**
     * Returns list of next photo for user view
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Photo> getPhotos() {

        List<Photo> photoList = photoDao.getPhotosList();
        return photoList;
    }

    /**
     * Upload photo on server
     * @param username
     * @param uploadedInputStream
     * @param contentDispositionHeader
     * @return
     * @throws IOException
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public Response uploadFile(
            @FormDataParam("username") String username,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition contentDispositionHeader) throws IOException {
        LOGGER.info("uploadFile is called");

        byte[] buffer = new byte[CHUNK_SIZE];

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        int read;
        while ((read = uploadedInputStream.read(buffer, 0, buffer.length)) > 0)
        {
            output.write(buffer, 0, read);
        }

        byte[] photoContent = output.toByteArray();

        User user = userService.getUser(username);

        Photo photo = new Photo();
        photo.setUserId(user.getId());
        photo.setPhoto(photoContent);

        photoService.addPhoto(photo);
        LOGGER.info("uploadFile has completed for username=" + username);

        return Response.ok()
                .entity("photoId=" + photo.getId())
                .build();
    }

    /**
     * Return paticular photo by its id
     * @param id
     * @return
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Photo getPhoto(@PathParam("id") String id) {

        LOGGER.info("download file with id=" + id);

        final Photo photo = photoService.getPhoto(id);

        if (photo == null || photo.getPhotoUrl().isEmpty()) {
            throw new NotFoundException("No photo with id=" + id + " found");
        }
        return photo;
    }

    /**
     * Make a like on photo
     * @param photoId
     * @return
     */
    @POST
    @Path("{id}/like")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Photo likePhoto(@PathParam("id") int photoId, UsernameDto userName) {

        User actionUser = userService.getUser(userName.getUsername());

        return photoService.likePhoto(new Long(photoId), new Long(actionUser.getId()));
    }

    /**
     * Make a dislike on photo
     * @param photoId
     * @return
     */
    @POST
    @Path("{id}/dislike")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Photo dislikePhoto(@PathParam("id") int photoId, UsernameDto userName) {

        User actionUser = userService.getUser(userName.getUsername());

        return photoService.dislikePhoto(new Long(photoId), new Long(actionUser.getId()));
    }

    /**
     * Returns nummber of likes
     * @param id
     * @return
     */
    @GET
    @Path("{id}/like")
    public int getLikes(@PathParam("id") String id) {
        LOGGER.info("get likes for id=" + id);

        final Photo photo = photoDao.getPhoto(Integer.parseInt(id));

        return photo.getUpVotes();
    }

    /**
     * Returns number of dislikes
     * @param id
     * @return
     */
    @GET
    @Path("{id}/dislike")
    public int getDislikes(@PathParam("id") String id) {
        LOGGER.info("get dislikes for id=" + id);

        final Photo photo = photoDao.getPhoto(Integer.parseInt(id));

        return photo.getDownVotes();
    }

    @POST
    @Path("{id}/report")
    public Response report(@PathParam("id") int photoId) {
        photoDao.report(photoId);

        return Response.ok().build();
    }

    public PhotoDao getPhotoDao() {
        return photoDao;
    }

    public void setPhotoDao(PhotoDao photoDao) {
        this.photoDao = photoDao;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public NotificationService getNotificationService() {
        return notificationService;
    }

    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    public PhotoService getPhotoService() {
        return photoService;
    }

    public void setPhotoService(PhotoService photoService) {
        this.photoService = photoService;
    }
}
