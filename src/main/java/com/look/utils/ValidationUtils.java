package com.look.utils;

import javax.validation.ConstraintViolation;
import java.util.Iterator;
import java.util.Set;

/**
 * Class contains the constants required for validation testing.
 * 
 */
public class ValidationUtils {

    public static boolean checkValidationResult(Set<? extends ConstraintViolation> violations, String violationPath) {
        boolean result = false;
        Iterator<? extends ConstraintViolation> it = violations.iterator();
        while (it.hasNext()) {
            if (violationPath.equals(it.next().getPropertyPath().toString())) {
                result = true;
                break;
            }
        }

        return result;
    }
}
