package com.look.model;

public class User {
    private long id;
    private String username;
    private String googleRegId;
    private String appleRegId;
    private String email;

    public User() {}

    public User(String username) {
        this.username = username;
    }

    public User(long id, String username, String googleRegId, String appleRegId, String email) {
        this.id = id;
        this.username = username;
        this.googleRegId = googleRegId;
        this.appleRegId = appleRegId;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGoogleRegId() {
        return googleRegId;
    }

    public void setGoogleRegId(String googleRegId) {
        this.googleRegId = googleRegId;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAppleRegId() {
        return appleRegId;
    }

    public void setAppleRegId(String appleRegId) {
        this.appleRegId = appleRegId;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", googleRegId='" + googleRegId + '\'' +
                ", appleRegId='" + appleRegId + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
