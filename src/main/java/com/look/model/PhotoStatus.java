package com.look.model;

/**
 * @Author: ivan
 * Date: 06.11.14
 * Time: 8:39
 */
public class PhotoStatus {

    public final static String UNKNOWN = "unknown";
    public final static String LIKED = "like";
    public final static String DISLIKED = "dislike";
}
