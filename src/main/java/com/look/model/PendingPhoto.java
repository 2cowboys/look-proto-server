package com.look.model;

/**
 * Class to represent a pending photo with all the information
 * needed to send notifications to users regarding this photo.
 */
public class PendingPhoto {
    private long userId;
    private String username;
    private String appleRegId;
    private long photoId;

    public PendingPhoto() {
    }

    public PendingPhoto(long userId, String username, String appleRegId, long photoId) {
        this.userId = userId;
        this.username = username;
        this.appleRegId = appleRegId;
        this.photoId = photoId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAppleRegId() {
        return appleRegId;
    }

    public void setAppleRegId(String appleRegId) {
        this.appleRegId = appleRegId;
    }

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }
}
