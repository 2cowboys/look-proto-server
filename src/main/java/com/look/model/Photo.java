package com.look.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @Author: ivan
 * Date: 11.09.14
 * Time: 20:07
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Photo {

    private long id;
    private long userId;
    private Integer upVotes;
    private Integer downVotes;
    @XmlTransient
    private Integer upVotesSince;
    @XmlTransient
    private Integer downVotesSince;
    @XmlTransient
    private byte[] photo;
    private String photoUrl;

    public Photo() {

    }

    public Photo(long id, long userId, Integer upVotes, Integer downVotes) {
        this.id = id;
        this.userId = userId;
        this.upVotes = upVotes;
        this.downVotes = downVotes;
        this.upVotesSince = 0;
        this.downVotesSince = 0;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Integer getDownVotes() {

        return downVotes;
    }

    public void setDownVotes(Integer downVotes) {
        this.downVotes = downVotes;
    }

    public Integer getUpVotes() {

        return upVotes;
    }

    public void setUpVotes(Integer upVotes) {
        this.upVotes = upVotes;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getUpVotesSince() {
        return upVotesSince;
    }

    public void setUpVotesSince(Integer upVotesSince) {
        this.upVotesSince = upVotesSince;
    }

    public Integer getDownVotesSince() {
        return downVotesSince;
    }

    public void setDownVotesSince(Integer downVotesSince) {
        this.downVotesSince = downVotesSince;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Photo photo = (Photo) o;

        if (id != photo.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", userId=" + userId +
                ", upVotes=" + upVotes +
                ", downVotes=" + downVotes +
                '}';
    }
}
