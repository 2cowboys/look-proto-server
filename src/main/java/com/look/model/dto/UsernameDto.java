package com.look.model.dto;

/**
 * @Author: ivan
 * Date: 07.11.14
 * Time: 9:05
 */
public class UsernameDto {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
