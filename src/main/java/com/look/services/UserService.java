package com.look.services;

import com.look.dao.UserDao;
import com.look.model.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class UserService {

    @Inject
    private UserDao userDao;

    public void createUser(User user) {
        if (user.getUsername() == null) {
            throw new IllegalArgumentException("Username can't be null");
        }

        userDao.createUser(user);
    }

    public User getUser(String username) {
        if (username == null) {
            throw new IllegalArgumentException("Username can't be null");
        }
        return userDao.getUser(username);
    }

    public User getUserById(long user_id) {
        return userDao.getUserById(user_id);
    }

    public List<User> getAllUsers() {
        return userDao.getListOfUsers();
    }

    public void removeUser(String username) {
        userDao.removeUser(username);
    }

    public List<User> getAllFakeUsers() {
        return userDao.getListOfFakeUsers();
    }
}
