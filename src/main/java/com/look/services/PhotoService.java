package com.look.services;

import com.look.dao.PhotoDao;
import com.look.model.Photo;
import com.look.model.PhotoStatus;
import com.look.model.User;
import com.look.model.PendingPhoto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

/**
 * @Author: ivan
 * Date: 03.10.14
 * Time: 0:39
 */
@Service
public class PhotoService {

    private static final Logger LOG = LoggerFactory.getLogger(PhotoService.class);

    @Inject
    PhotoDao photoDao;
    @Inject
    UserService userService;
    @Inject
    S3Service s3Service;

    public void addPhoto(Photo photo) {

        String photoUrl = s3Service.saveFileToS3(UUID.randomUUID().toString(), photo.getPhoto());
        photo.setPhotoUrl(photoUrl);
        photoDao.addPhoto(photo);

        List<User> users = userService.getAllUsers();
        for (User user:users) {
            if (photo.getUserId() == user.getId()) {
                continue;
            }
            photoDao.pendPhotoForUser(photo.getId(), user.getId(), photo.getUserId());
        }
    }

    public Photo likePhoto(Long photoId, Long userId) {
        return actionWithPhoto(photoId, userId, PhotoStatus.LIKED);
    }

    public Photo dislikePhoto(Long photoId, Long userId) {
        return actionWithPhoto(photoId, userId, PhotoStatus.DISLIKED);
    }

    protected Photo actionWithPhoto(Long photoId, Long userId, String status) {
        if (status.equals(PhotoStatus.LIKED)) {
            photoDao.likePhoto(photoId);
        }else{
            photoDao.dislikePhoto(photoId);
        }
        photoDao.updatePendingPhotoStatus(photoId, userId, status);

        Photo photo = photoDao.getPhoto(photoId);

        return photo;
    }

    public Photo getPhoto(String id) {

        Photo photo = photoDao.getPhoto(Integer.parseInt(id));
        return photo;
    }

    public PhotoDao getPhotoDao() {
        return photoDao;
    }

    public void setPhotoDao(PhotoDao photoDao) {
        this.photoDao = photoDao;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public S3Service getS3Service() {
        return s3Service;
    }

    public void setS3Service(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    public List<Photo> getUserPendingPhotos(User user) {
        return photoDao.getUserPendingPhotos(user.getId());
    }

    public List<PendingPhoto> getPendingPhotos() {
        return photoDao.getPendingPhotos();
    }

    public void updatePendingPhotoNotificationStatus(Long photoId, Long userId) {
        photoDao.updatePendingPhotoNotificationStatus(photoId, userId);
    }
}
