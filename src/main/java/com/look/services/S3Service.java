package com.look.services;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.util.UUID;

/**
 * @Author: ivan
 * Date: 11.11.14
 * Time: 23:31
 */
@Service
public class S3Service {

    private static final Logger LOG = LoggerFactory.getLogger(S3Service.class);
    private static final String bucketName = "look-proto-local";
    private AmazonS3 s3;

    public S3Service() {
        AWSCredentials credentials = new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return "AKIAJ7NRQHX75MWGEZIQ";
            }

            @Override
            public String getAWSSecretKey() {
                return "9gMnItpihKbxqDrOMjP/1pxxCbKGs644x258pAyA";
            }
        };
        s3 = new AmazonS3Client(credentials);
    }

    public String saveFileToS3(String key, byte[] fileContent) {

        String fileURL = "";

        try {
            LOG.info("Uploading a new object to S3 from a file ");
            InputStream inputStream = new ByteArrayInputStream(fileContent);

            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("image/jpeg");
            metadata.setContentLength(fileContent.length);
            metadata.setCacheControl("private");

            s3.putObject(new PutObjectRequest(
                    bucketName, key, inputStream, metadata));
            s3.setObjectAcl(bucketName, key, CannedAccessControlList.PublicRead);

            fileURL = "http://s3.amazonaws.com/" + bucketName + "/" + key;

        } catch (AmazonServiceException ase) {
            LOG.error("Caught an AmazonServiceException, which " +
                    "means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            LOG.error("Error Message:    " + ase.getMessage());
            LOG.error("HTTP Status Code: " + ase.getStatusCode());
            LOG.error("AWS Error Code:   " + ase.getErrorCode());
            LOG.error("Error Type:       " + ase.getErrorType());
            LOG.error("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            LOG.error("Caught an AmazonClientException, which " +
                    "means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            LOG.error("Error Message: " + ace.getMessage());
        }
        return fileURL;
    }
}
