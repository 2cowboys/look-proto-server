package com.look.services.push;

import com.look.services.NotificationService;
import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A connector interface to the Apple Push Notifications service.
 */
public class APNConnector {

    private static final String PUSH_CERTIFICATE = "push/ios-prod.p12";
    private static final String PUSH_CERTIFICATE_PASSWORD = "$chvvAr2eneGGer";
    private static final String NOTIFICATION_SOUND = "default";
    private static Logger LOG = LoggerFactory.getLogger(APNConnector.class);

    public void sendMessage(NotificationService.PushMessage message, String recipientId) throws PushDeliveryException {
        LOG.debug("Start processing recipient {}", recipientId);
        try {
            int badge = 1; //make it one for now
            sendMessage(message, badge, recipientId);
        } catch (Exception e) {
            LOG.error("Unable to deliver message to APNS: {}\n", recipientId, e);
        }
        LOG.debug("End processing recipient {}", recipientId);
    }
    
    void sendMessage(NotificationService.PushMessage alert, int badge, String registrationId) throws CommunicationException, KeystoreException, JSONException {
        PushNotificationPayload payload = PushNotificationPayload.complex();
        payload.addAlert(alert.getMessage());
        payload.addBadge(badge);
        payload.addSound(NOTIFICATION_SOUND);
        payload.addCustomDictionary("type", alert.getType());
        payload.addCustomDictionary("photoId", alert.getPhotoId());
        Push.payload(
                payload,
                APNConnector.class.getClassLoader().getResourceAsStream(PUSH_CERTIFICATE),
                PUSH_CERTIFICATE_PASSWORD,
                true,
                registrationId);
    }


}
