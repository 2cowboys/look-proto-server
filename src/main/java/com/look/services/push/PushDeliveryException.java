package com.look.services.push;

/**
 * Exception indicating problems with
 */
public class PushDeliveryException extends Exception {

    public PushDeliveryException() {
    }

    public PushDeliveryException(String message) {
        super(message);
    }

    public PushDeliveryException(String message, Throwable cause) {
        super(message, cause);
    }

    public PushDeliveryException(Throwable cause) {
        super(cause);
    }

    public PushDeliveryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
