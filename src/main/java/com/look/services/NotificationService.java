package com.look.services;

import com.look.dao.UserDao;
import com.look.model.Photo;
import com.look.model.User;

import java.util.List;

import com.look.services.push.APNConnector;
import com.look.services.push.PushDeliveryException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * @Author: ivan
 * Date: 22.09.14
 * Time: 0:51
 */

@Service
public class NotificationService {

    private static final Logger LOG = LoggerFactory.getLogger(NotificationService.class);

    @Inject
    private UserDao userDao;
    private APNConnector apnConnector;

    public static class PushMessage {
        private final String type;
        private final String message;
        private final String photoId;

        public PushMessage(String photoId, String message, String type) {
            this.photoId = photoId;
            this.message = message;
            this.type = type;
        }

        public String getPhotoId() {
            return photoId;
        }

        public String getMessage() {
            return message;
        }

        public String getType() {
            return type;
        }

        @Override
        public String toString() {
            return "PushMessage{" +
                    "type='" + type + '\'' +
                    ", message='" + message + '\'' +
                    ", photoId='" + photoId + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PushMessage that = (PushMessage) o;

            if (!message.equals(that.message)) return false;
            if (!photoId.equals(that.photoId)) return false;
            if (!type.equals(that.type)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = type.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + photoId.hashCode();
            return result;
        }
    }

    public NotificationService() {
        apnConnector = new APNConnector();
    }

    public void notifyRecipients(User user, Photo photo) {
        List<User> users = userDao.getListOfUsers();

        for (User recipient : users) {
            LOG.info("Process user: " + recipient);
            if (recipient.equals(user)) {
                continue;
            }
            sendMessage(recipient, getPendingPhotoNotificationMessage(photo));
        }
    }

    private PushMessage getPendingPhotoNotificationMessage(Photo photo) {
        return getNotificationMessage(photo);
    }

    public PushMessage getNotificationMessage(Photo photo) {
        return new PushMessage(String.valueOf(photo.getId()), "You've got a new photo to like", "pending");
    }

    private String getBulkNotificationMessage(Photo photo) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("You've got new ");
        boolean needComma = false;
        if (photo.getUpVotesSince() > 0) {
            stringBuilder.append(photo.getUpVotesSince() + " ups");
            needComma = true;
        }

        if (photo.getDownVotesSince() > 0) {
            if (needComma) {
                stringBuilder.append(", ");
            }
            stringBuilder.append(photo.getDownVotesSince() + " downs");
        }

        return stringBuilder.toString();
    }

    public void sendBulkNotificationsAboutPhoto(User recipient, Photo photo) {
        sendMessage(recipient, new PushMessage(String.valueOf(photo.getId()), getBulkNotificationMessage(photo), "like"));
    }

    public void sendMessage(User recipient, PushMessage content) {
        try {
            LOG.info("Message to user {} is about to be sent", recipient.getUsername());
            if (recipient.getGoogleRegId() == null && recipient.getAppleRegId() == null) {
                throw new PushDeliveryException("Both google and apple reg ids are null");
            }
            if (recipient.getAppleRegId() != null) {
                sendMessageToApple(content, recipient.getAppleRegId());
            }
            LOG.info("Message to user {} has been sent", recipient.getUsername());
        } catch (PushDeliveryException e) {
            LOG.error("Message to user {} failed to be sent", recipient.getUsername(), e);
        }
    }

    private void sendMessageToApple(PushMessage content, String recipientId) throws PushDeliveryException {
        apnConnector.sendMessage(content, recipientId);
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
