package com.look.jobs;

import com.look.model.PendingPhoto;
import com.look.model.User;
import com.look.services.NotificationService;
import com.look.services.NotificationService.PushMessage;
import com.look.services.PhotoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

/**
 * @Author: george
 * Date: 27.11.14
 * Time: 0:34
 */
public class PendingJob {

    private static final Logger LOG = LoggerFactory.getLogger(PendingJob.class);

    @Inject
    PhotoService photoService;
    @Inject
    NotificationService notificationService;

    public void executeJob() {
        LOG.info("Pending job started");

        List<PendingPhoto> pendingPhotos = photoService.getPendingPhotos();

        for (PendingPhoto photo : pendingPhotos) {
            notificationService.sendMessage(
                    new User(photo.getUserId(), photo.getUsername(), null, photo.getAppleRegId(), null),
                    new PushMessage(String.valueOf(photo.getPhotoId()), "You've got a new photo to like", "pending"));
            photoService.updatePendingPhotoNotificationStatus(photo.getPhotoId(), photo.getUserId());
        }

        LOG.info("Pending job completed");
    }

    public PhotoService getPhotoService() {
        return photoService;
    }

    public void setPhotoService(PhotoService photoService) {
        this.photoService = photoService;
    }

    public NotificationService getNotificationService() {
        return notificationService;
    }

    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
}
