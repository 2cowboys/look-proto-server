package com.look.jobs;

import com.look.dao.PhotoDao;
import com.look.model.Photo;
import com.look.model.User;
import com.look.services.NotificationService;
import com.look.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

/**
 * @Author: ivan
 * Date: 05.02.15
 * Time: 21:21
 */
public class NotificationJob {

    private static final Logger LOG = LoggerFactory.getLogger(LikesJob.class);
    @Inject
    private UserService userService;
    @Inject
    private PhotoDao photoDao;
    @Inject
    private NotificationService notificationService;

    public void executeJob() {

        LOG.info("Notification scheduller works...");
        System.out.println("Notification scheduller works...");

        List<Photo> photosForNotification = photoDao.getPhotosForNotification();
        for (Photo photo:photosForNotification) {

            LOG.info("Going to work with photo for bulk update: " + photo.getId());

            User user = userService.getUserById(photo.getUserId());

            LOG.info("Send bulk notification for photo: " + photo.getId());
            notificationService.sendBulkNotificationsAboutPhoto(user, photo);
            photoDao.resetPhotoNotificationStat(photo.getId(), photo.getUpVotesSince(), photo.getDownVotesSince());
        }
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public PhotoDao getPhotoDao() {
        return photoDao;
    }

    public void setPhotoDao(PhotoDao photoDao) {
        this.photoDao = photoDao;
    }

    public NotificationService getNotificationService() {
        return notificationService;
    }

    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
}
