package com.look.jobs;

import com.look.dao.PhotoDao;
import com.look.model.Photo;
import com.look.model.PhotoStatus;
import com.look.model.User;
import com.look.services.PhotoService;
import com.look.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;
import java.util.Random;

/**
 * @Author: ivan
 * Date: 27.11.14
 * Time: 0:34
 */
public class LikesJob {

    private static final Logger LOG = LoggerFactory.getLogger(LikesJob.class);
    public static final long MAIN_USER_ID = 442L;
    @Inject
    UserService userService;
    @Inject
    PhotoService photoService;
    @Inject
    PhotoDao photoDao;

    public void executeJob() {

        LOG.info("Like scheduller works...");
        System.out.println("Like scheduller works...");

        Random rnd = new Random();
        List<User> fakeUsers = userService.getAllFakeUsers();
        for (User user:fakeUsers) {
            LOG.info("Going to work with user: " + user.getUsername() + "(" + user.getId() + ")");
            List<Photo> photos = photoDao.getUserPendingPhotos(user.getId());
            for (Photo photo:photos) {

                LOG.info("Going to work with photo: " + photo.getId());

                String photoStatus = StringUtils.defaultIfEmpty(
                        photoDao.getPhotoStatus(photo.getId(), MAIN_USER_ID),
                        PhotoStatus.UNKNOWN);

                int term = 0;

                switch (photoStatus) {
                    case PhotoStatus.UNKNOWN: term = 0; break;
                    case PhotoStatus.LIKED: term = 30; break;
                    case PhotoStatus.DISLIKED: term = -30; break;
                }

                if (rnd.nextInt(100) + term >= 50) {
                    photoService.likePhoto(photo.getId(), user.getId());
                    LOG.info("Work with photo: " + photo.getId() + ", Liked");
                } else {
                    photoService.dislikePhoto(photo.getId(), user.getId());
                    LOG.info("Work with photo: " + photo.getId() + ", Disliked");
                }
                int timeout = rnd.nextInt(10000);//up to 10 seconds
                LOG.info("Wait for " + timeout + "ms");
                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException e) {
                    LOG.info("Wait was interrupted {}", e.getMessage());
                }
            }
        }
    }
}
