package com.look.jobs;

import com.look.dao.PhotoDao;
import com.look.model.Photo;
import com.look.model.User;
import com.look.services.NotificationService;
import com.look.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;

/**
 * @Author: ivan
 * Date: 07.02.15
 * Time: 18:27
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
public class NotificationJobTest {

    @Mock
    PhotoDao mockPhotoDao;
    @Mock
    NotificationService mockNotificationService;
    @Mock
    UserService mockUserService;

    NotificationJob notificationJob;

    @Before
    public void setup() {
        notificationJob = new NotificationJob();
        notificationJob.setPhotoDao(mockPhotoDao);
        notificationJob.setNotificationService(mockNotificationService);
        notificationJob.setUserService(mockUserService);
    }

    @Test
    public void testNotificationJob() {

        List<Photo> photoList = new ArrayList<Photo>();
        Photo photo1 = new Photo(1,2,3,4);
        Photo photo2 = new Photo(2,2,3,4);
        photo1.setUpVotesSince(4);
        photo1.setUpVotesSince(5);
        photo2.setUpVotesSince(6);
        photo2.setUpVotesSince(7);
        photoList.add(photo1);
        photoList.add(photo2);

        User user = new User(2, "username", null, null, null);

        when(mockUserService.getUserById(2)).thenReturn(user);
        when(mockPhotoDao.getPhotosForNotification()).thenReturn(photoList);

        notificationJob.executeJob();

        verify(mockUserService, times(2)).getUserById(2);
        verify(mockPhotoDao).getPhotosForNotification();
        verify(mockPhotoDao).resetPhotoNotificationStat(photo1.getId(), photo1.getUpVotesSince(), photo1.getDownVotesSince());
        verify(mockPhotoDao).resetPhotoNotificationStat(photo2.getId(), photo2.getUpVotesSince(), photo2.getDownVotesSince());
        verify(mockNotificationService).sendBulkNotificationsAboutPhoto(user, photo1);
        verify(mockNotificationService).sendBulkNotificationsAboutPhoto(user, photo2);

    }
}
