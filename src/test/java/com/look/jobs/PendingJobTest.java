package com.look.jobs;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.look.model.User;
import com.look.services.NotificationService;
import com.look.services.NotificationService.PushMessage;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;

import static org.mockito.Matchers.eq;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
@DatabaseSetup(
        value = "classpath:/com/look/dao/database_empty.xml",
        type = DatabaseOperation.DELETE_ALL)
public class PendingJobTest {

    @MockitoAnnotations.Mock
    NotificationService notificationService;

    @Inject
    @InjectMocks
    PendingJob pendingJob;

    @Before
    public void setUp() {
        notificationService = Mockito.mock(NotificationService.class);
        pendingJob.setNotificationService(notificationService);
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT,
            value = {
                    "classpath:/com/look/dao/photos_status_userid22_1.xml",
                    "classpath:/com/look/dao/photos_status_userid12_4.xml",
                    "classpath:/com/look/dao/users_id12.xml",
                    "classpath:/com/look/dao/users_id22.xml"
            })
    public void testExecuteJob() throws Exception {
        pendingJob.executeJob();

        Mockito.verify(notificationService).sendMessage(eqUser(createUser(12L)), eq(createPendingMessage(1201L)));
        Mockito.verify(notificationService).sendMessage(eqUser(createUser(12L)), eq(createPendingMessage(1202L)));
        Mockito.verify(notificationService).sendMessage(eqUser(createUser(12L)), eq(createPendingMessage(1203L)));
        Mockito.verify(notificationService).sendMessage(eqUser(createUser(22L)), eq(createPendingMessage(2201L)));
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT,
            value = {
                    "classpath:/com/look/dao/photos_status_userid22_1.xml",
                    "classpath:/com/look/dao/photos_status_userid12_4.xml",
                    "classpath:/com/look/dao/users_id12.xml",
                    "classpath:/com/look/dao/users_id22.xml"
            })
    public void testRunJobTwice() throws Exception {
        pendingJob.executeJob();
        Mockito.reset(notificationService);
        pendingJob.executeJob();

        Mockito.verifyZeroInteractions(notificationService);
    }

    private PushMessage createPendingMessage(long l) {
        return new PushMessage(String.valueOf(l), "You've got a new photo to like", "pending");
    }

    private User createUser(long id) {
        return new User(id, "test" + id, null, "apple" + id, null);
    }

    private User eqUser(final User value) {
        return Mockito.argThat(new BaseMatcher<User>() {
            @Override
            public void describeTo(Description description) {
                description.appendValue(value);
            }

            @Override
            public boolean matches(Object item) {
                User user = (User) item;
                return value.getId() == user.getId()
                        && StringUtils.equals(value.getUsername(), user.getUsername())
                        && StringUtils.equals(value.getAppleRegId(), user.getAppleRegId());
            }

        });
    }
}