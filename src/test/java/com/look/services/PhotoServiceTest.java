package com.look.services;

import com.look.dao.PhotoDao;
import com.look.model.Photo;
import com.look.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @Author: ivan
 * Date: 05.10.14
 * Time: 16:32
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
public class PhotoServiceTest {

    @Mock
    PhotoDao mockPhotoDao;
    @Mock
    UserService mockUserService;
    @Mock
    S3Service s3Service;

    PhotoService photoService;

    @Before
    public void setup() {
        photoService = new PhotoService();
        photoService.setPhotoDao(mockPhotoDao);
        photoService.setUserService(mockUserService);
        photoService.setS3Service(s3Service);
    }

    @Test
    public void testAddPhoto() {
        Photo photoToAdd = new Photo(12, 2, 5, 7);

        User user1 = new User(1, "user1", "regId1", null, null);
        User user2 = new User(2, "user2", "regId2", null, null);
        List<User> users = new ArrayList<User>();
        users.add(user1);
        users.add(user2);
        when(mockUserService.getAllUsers()).thenReturn(users);

        photoService.addPhoto(photoToAdd);

        verify(mockPhotoDao).pendPhotoForUser(12, 1, 2);
        verify(mockPhotoDao, never()).pendPhotoForUser(12, 2, 2);
    }

    @Test
    public void testLikePhotos() {
        Photo photo = new Photo(1,2,3,4);
        List<Photo> photoList = new ArrayList<Photo>();
        photoList.add(photo);

        when(mockPhotoDao.getPhoto(5)).thenReturn(photo);

        Photo result = photoService.likePhoto(5L, 10L);

        assertNotNull(result);
        verify(mockPhotoDao).getPhoto(5);
        verify(mockPhotoDao).likePhoto(5);
    }

    @Test
    public void testDislikePhotos() {
        Photo photo = new Photo(1,2,3,4);
        List<Photo> photoList = new ArrayList<Photo>();
        photoList.add(photo);

        when(mockPhotoDao.getPhoto(5)).thenReturn(photo);

        Photo result = photoService.dislikePhoto(5L, 10L);

        assertNotNull(result);
        verify(mockPhotoDao).getPhoto(5);
        verify(mockPhotoDao).dislikePhoto(5);
    }
}
