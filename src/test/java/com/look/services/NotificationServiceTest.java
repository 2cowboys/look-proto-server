package com.look.services;

import com.look.dao.UserDao;
import com.look.webservices.resources.PhotoResource;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import static org.junit.Assert.fail;

/**
 * @Author: ivan
 * Date: 22.09.14
 * Time: 1:11
 */

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
public class NotificationServiceTest {

    NotificationService notificationService;
    @Mock
    UserDao userDao;

    @Before
    public void setup() {
        notificationService = new NotificationService();
        notificationService.setUserDao(userDao);
    }

    @Test
    @Ignore
    public void testNotifyRecipients() {
        fail("Implement test!");
    }
}
