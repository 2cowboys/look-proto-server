package com.look.webservices.resources;

import com.look.dao.PhotoDao;
import com.look.model.dto.UsernameDto;
import com.look.services.PhotoService;
import com.look.services.UserService;
import com.look.model.Photo;
import com.look.model.User;
import com.look.services.NotificationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;

import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.Mock;
import static org.hamcrest.Matchers.is;

/**
 * @Author: ivan
 * Date: 13.09.14
 * Time: 1:36
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
public class PhotoResourceTest {

    PhotoResource photoResource;
    @Mock
    PhotoDao mockPhotoDao;
    @Mock
    UserService mockUserService;
    @Mock
    PhotoService mockPhotoService;
    @Mock
    NotificationService mockNotificationService;

    @Before
    public void setup() {
        photoResource = new PhotoResource();
        photoResource.setPhotoDao(mockPhotoDao);
        photoResource.setUserService(mockUserService);
        photoResource.setNotificationService(mockNotificationService);
        photoResource.setPhotoService(mockPhotoService);
    }

    @Test
    public void testGetAllPhotos() {

        Photo photo = new Photo(1,2,3,4);
        List<Photo> photoList = new ArrayList<Photo>();
        photoList.add(photo);

        when(mockPhotoDao.getPhotosList()).thenReturn(photoList);

        List<Photo> result = photoResource.getPhotos();

        assertThat(result.size(), is(1));
        verify(mockPhotoDao).getPhotosList();
    }

    @Test
    public void testGetPhoto() {
        Photo photo = new Photo(1,2,3,4);
        byte[] photoBytes = new byte[] {1,1,2,2,0};
        photo.setPhoto(photoBytes);
        photo.setPhotoUrl("testUrl1");

        when(mockPhotoService.getPhoto("5")).thenReturn(photo);

        Photo result = photoResource.getPhoto("5");

        assertNotNull(result);
        verify(mockPhotoService).getPhoto("5");
    }

    @Test
    public void testLikePhotos() {

        User actionUser = new User(10, "action", "", null, null);

        Photo photo = new Photo(1,2,3,4);

        when(mockUserService.getUser("action")).thenReturn(actionUser);
        when(mockPhotoService.likePhoto(photo.getId(), actionUser.getId())).thenReturn(photo);

        UsernameDto dto = new UsernameDto();
        dto.setUsername("action");

        Photo result = photoResource.likePhoto(1, dto);

        assertNotNull(result);
        verify(mockPhotoService).likePhoto(1L, 10L);
        verify(mockUserService).getUser("action");
    }

    @Test
    public void testDislikePhotos() {

        User actionUser = new User(10, "action", "", null, null);

        Photo photo = new Photo(1,2,3,4);

        when(mockUserService.getUser("action")).thenReturn(actionUser);
        when(mockPhotoService.dislikePhoto(photo.getId(), actionUser.getId())).thenReturn(photo);

        UsernameDto dto = new UsernameDto();
        dto.setUsername("action");

        Photo result = photoResource.dislikePhoto(1, dto);

        assertNotNull(result);
        verify(mockPhotoService).dislikePhoto(1L, 10L);
        verify(mockUserService).getUser("action");
    }

    @Test
    public void testUploadPhoto() throws Exception {

        String fileContent = "File content";
        byte[] byteContent = fileContent.getBytes();
        InputStream stream = new ByteArrayInputStream(byteContent);

        User user = new User("testUser");
        user.setId(1L);

        doReturn(user).when(mockUserService).getUser("testUser");
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Photo photo = (Photo) invocation.getArguments()[0];
                photo.setId(11);

                return 1;
            }
        }).when(mockPhotoService).addPhoto(any(Photo.class));

        Response result = photoResource.uploadFile("testUser", stream, null);

        assertThat(result.getStatus(), is(200));
        assertThat((String)result.getEntity(), is("photoId=11"));

        verify(mockUserService).getUser("testUser");
        verify(mockPhotoService).addPhoto(any(Photo.class));
    }
}
