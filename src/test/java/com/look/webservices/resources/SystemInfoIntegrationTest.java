package com.look.webservices.resources;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.look.webservices.config.LookApplication;
import com.restfb.FacebookClient;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.client.Entity.json;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
public class SystemInfoIntegrationTest extends JerseyTest {

    @Override
    protected Application configure() {
        ResourceConfig config = new LookApplication();
        config.property("contextConfigLocation", "classpath:/spring/applicationContext.xml");
        return config;
    }

    @Test
    public void testSystemInfoShouldAllowUnauthorizedAccess() {
        // we do not provide access token here
        Response response = target("/systeminfo").request().get();

        assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
    }

}