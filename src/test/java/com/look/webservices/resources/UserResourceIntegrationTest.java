package com.look.webservices.resources;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.look.webservices.config.LookApplication;
import com.restfb.FacebookClient;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import static javax.ws.rs.client.Entity.json;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
@DatabaseTearDown(
        type = DatabaseOperation.DELETE_ALL,
        value = "classpath:/com/look/dao/tearDownAll.xml")
public class UserResourceIntegrationTest extends AbstractIntegrationTest {

    @Test
    @DatabaseSetup(type = DatabaseOperation.DELETE_ALL, value = "classpath:/com/look/dao/createUserSetUpDataset.xml")
    public void testCreateUserAndGet() {
        authorizedRequest("/user").post(Entity.json("{\"username\": \"harold\", \"googleRegId\": \"12345\"}"));
        String userJson = authorizedRequest("/user/harold").get(String.class);
        assertThat(userJson, Matchers.containsString("\"username\":\"harold\""));
        assertThat(userJson, Matchers.containsString("\"googleRegId\":\"12345\""));
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT, value = "classpath:/com/look/dao/getUserPhotosSetUpDataset.xml")
    public void testGetUserPhotos() {
        String userPhotosJson = authorizedRequest("/user/testUserName/photos").get(String.class);
        assertThat(userPhotosJson, Matchers.containsString("{\"id\":3,\"userId\":12,\"upVotes\":5,\"downVotes\":7,\"photoUrl\":\"test_url1\"}," +
                "{\"id\":4,\"userId\":12,\"upVotes\":10,\"downVotes\":1,\"photoUrl\":\"test_url2\"}"));
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT, value = "classpath:/com/look/dao/createPendingPhotoSetUpDataset.xml")
    public void testGetUserPendingPhotos() {
        String userPendingPhotosJson = authorizedRequest("/user/testUserName/photos/pending").get(String.class);
        assertThat(userPendingPhotosJson, Matchers.containsString(
                "{\"id\":4,\"userId\":12,\"upVotes\":78,\"downVotes\":8,\"photoUrl\":\"test_url2\"},"
                + "{\"id\":3,\"userId\":12,\"upVotes\":5,\"downVotes\":7,\"photoUrl\":\"test_url1\"}"));
    }

    public void testSuccessfulLogin() {
        authorizedRequest("/user")
                .post(json("{\"username\": \"harold\"" +
                        ", \"googleRegId\": \"12345\"" +
                        ", \"email\": \"harold@mail.com\"}"));

        String userJson = authorizedRequest("/user/harold").get(String.class);

        assertThat(userJson, containsString("\"username\":\"harold\""));
        assertThat(userJson, containsString("\"googleRegId\":\"12345\""));
    }

    @Test
    public void testUnsuccessfulLogin() {
        // we do not provide access token here
        Response response = target("/user").request()
                .post(json("{\"username\": \"harold\"" +
                        ", \"googleRegId\": \"12345\"" +
                        ", \"email\": \"harold@mail.com\"}"));


        assertThat(response.getStatus(), is(Response.Status.UNAUTHORIZED.getStatusCode()));
    }

    @Test
    @DatabaseSetup(
            value = "classpath:/com/look/dao/createUserExpectedDataset.xml",
            type = DatabaseOperation.CLEAN_INSERT)
    public void testDeleteUserAndGet() {
        authorizedRequest("/user/test").delete();
        Response response = authorizedRequest("/user/test").get();

        assertThat(response.getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
    }

}