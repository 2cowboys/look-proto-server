package com.look.webservices.resources;

import com.look.dao.PhotoDao;
import com.look.model.Photo;
import com.look.model.User;
import com.look.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @Author: ivan
 * Date: 24.09.14
 * Time: 0:02
 */

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
public class UserResourceTest {

    @InjectMocks
    UserResource userResource;
    @Mock
    UserService mockUserService;
    @Mock
    PhotoDao mockPhotoDao;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetUserPhotos() throws Exception {

        final String USERNAME = "username";
        final Long USER_ID = 12L;

        User user = new User(USER_ID, USERNAME, "123", null, null);
        List<Photo> photos = new ArrayList<Photo>();
        photos.add(new Photo(1,2,3,4));
        photos.add(new Photo(1,2,3,4));

        when(mockUserService.getUser(USERNAME)).thenReturn(user);
        when(mockPhotoDao.getUserPhotos(USER_ID)).thenReturn(photos);

        List<Photo> resultList = userResource.getUserPhotos(USERNAME);

        assertThat(resultList.size(), is(photos.size()));
        assertThat(resultList.get(0), is(photos.get(0)));
        assertThat(resultList.get(1), is(photos.get(1)));

        verify(mockPhotoDao).getUserPhotos(USER_ID);
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserPhotosWithException() throws Exception {

        final String USERNAME = "username";
        User user = null;

        when(mockUserService.getUser(USERNAME)).thenReturn(user);

        List<Photo> resultList = userResource.getUserPhotos(USERNAME);

        assertNull(resultList);
    }

    @Test
    public void testGetUserPendingPhotos() throws Exception {

        final String USERNAME = "username";
        final Long USER_ID = 12L;

        User user = new User(USER_ID, USERNAME, "123", null, null);
        List<Photo> photos = new ArrayList<Photo>();
        photos.add(new Photo(1,2,3,4));
        photos.add(new Photo(1,2,3,4));

        when(mockUserService.getUser(USERNAME)).thenReturn(user);
        when(mockPhotoDao.getUserPendingPhotos(USER_ID)).thenReturn(photos);

        List<Photo> resultList = userResource.getUserPendingPhotos(USERNAME);

        assertThat(resultList.size(), is(photos.size()));
        assertThat(resultList.get(0), is(photos.get(0)));
        assertThat(resultList.get(1), is(photos.get(1)));

        verify(mockPhotoDao).getUserPendingPhotos(USER_ID);
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserPendingPhotosWithException() throws Exception {

        final String USERNAME = "username";
        User user = null;

        when(mockUserService.getUser(USERNAME)).thenReturn(user);

        List<Photo> resultList = userResource.getUserPendingPhotos(USERNAME);

        assertNull(resultList);
    }
}
