package com.look.webservices.resources;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @Author: ivan
 * Date: 17.09.14
 * Time: 21:00
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(locations = {
        "/spring/applicationContext.xml"
})
@DatabaseSetup(value = "classpath:/com/look/dao/database_empty.xml",
        type = DatabaseOperation.DELETE_ALL)
public class PhotoResourceIntegrationTest extends AbstractIntegrationTest {

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT,
            value = {
                    "classpath:/com/look/dao/users_id12.xml",
                    "classpath:/com/look/dao/users_id22.xml",
                    "classpath:/com/look/dao/photos_userid12_1.xml",
                    "classpath:/com/look/dao/photos_status_userid22_1.xml"
            })
    public void testLikes() {

        String pendingResponse = authorizedRequest("/user/test22/photos/pending").get(String.class);

        assertThat(pendingResponse, is(not("[]")));

        String userJson = authorizedRequest("/photo/2201/like")
                .post(Entity.json("{\"username\": \"test22\"}"), String.class);
        assertThat(userJson, Matchers.equalTo("{\"id\":2201,\"userId\":12,\"upVotes\":1,\"downVotes\":0}"));

        pendingResponse = authorizedRequest("/user/test22/photos/pending").get(String.class);

        assertThat(pendingResponse, is("[]"));
//
//        userJson = authorizedRequest("/photo/3/dislike")
//                .post(Entity.json("{\"username\": \"harold\"}"), String.class);
//        assertThat(userJson, Matchers.equalTo("{\"id\":3,\"userId\":12,\"upVotes\":6,\"downVotes\":8,\"photoUrl\":\"url\"}"));
    }

    @Test
    @DatabaseSetup(value = "classpath:/com/look/dao/createPhotoSetUpDataset.xml",
        type = DatabaseOperation.CLEAN_INSERT)
    @ExpectedDatabase(value = "classpath:/com/look/dao/reportPhotoExpectedDataset.xml",
        table = "photos",
        assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testReport() {
        Response response1 = authorizedRequest("/photo/3/report").post(null);
        Response response2 = authorizedRequest("/photo/3/report").post(null);

        assertThat(response1.getStatus(), is(Response.Status.OK.getStatusCode()));
        assertThat(response2.getStatus(), is(Response.Status.OK.getStatusCode()));
    }

    @Test
    @DatabaseSetup(value =
            {
                    "classpath:/com/look/dao/photos_empty.xml",
                    "classpath:/com/look/dao/users_id12.xml"
            },
            type = DatabaseOperation.CLEAN_INSERT)
    @ExpectedDatabase(value = "classpath:/com/look/dao/photos_userid12_1.xml",
            table = "photos",
            query = "select 2201 as id, user_id, up_votes, down_votes, reported from photos",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testAddPhoto() {
        final MultiPart multipart = new FormDataMultiPart()
                .field("username", "test12")
                .bodyPart(new StreamDataBodyPart("file",
                        new ByteArrayInputStream(new byte[]{1, 2, 3, 4}), "test.jpg", MediaType.valueOf("image/jpeg")));
        Response response = authorizedRequest("/photo")
                .accept(MediaType.MEDIA_TYPE_WILDCARD)
                .post(Entity.entity(multipart, multipart.getMediaType()));

        String result = response.readEntity(String.class);
        String photoId = result.split("=")[1];

        String photoInfo = authorizedRequest("/photo/" + photoId)
                .accept(MediaType.MEDIA_TYPE_WILDCARD)
                .get(String.class);
        Matcher matcher = Pattern.compile(".*\"photoUrl\":\"([^\"]+)\".*").matcher(photoInfo);
        matcher.matches();
        String photoUrl = matcher.group(1);
        Response amazonResponse = client()
                .target(photoUrl)
                .request(MediaType.MEDIA_TYPE_WILDCARD)
                .get();

        assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
        assertThat(photoInfo, not(isEmptyOrNullString()));
        assertThat(amazonResponse.getHeaderString(HttpHeaders.CONTENT_TYPE), is("image/jpeg"));
        assertThat(amazonResponse.getLength(), is(4));
    }
}
