package com.look.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.look.model.User;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@ContextConfiguration(locations = {
    "/spring/applicationContext.xml"
})
@DatabaseTearDown(
        value = "tearDownAll.xml",
        type = DatabaseOperation.DELETE_ALL)
public class UserDaoTest {

    @Inject
    private UserDao userDao;

    @Test
    @DatabaseSetup(
            value = "createUserSetUpDataset.xml",
            type = DatabaseOperation.DELETE_ALL)
    @ExpectedDatabase(
            value = "createUserExpectedDataset.xml",
            table = "users",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testCreateUser() throws Exception {
        User user = new User();
        user.setUsername("test");
        user.setGoogleRegId("12345");
        user.setEmail("test@email.com");
        user.setAppleRegId("apple12345");

        userDao.createUser(user);

        user = new User();
        user.setUsername("test2");
        user.setGoogleRegId("54321");
        user.setEmail("test2@email.com");

        userDao.createUser(user);
    }

    @Test
    @DatabaseSetup(
            value = "createUserExpectedDataset.xml",
            type = DatabaseOperation.CLEAN_INSERT)
    @ExpectedDatabase(
            value = "createUserExpectedDataset.xml",
            table = "users",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testCreateUserWhenUserAlreadyExists() throws Exception {
        User user = new User();
        user.setUsername("test");
        user.setGoogleRegId("12345");
        user.setEmail("test@email.com");
        user.setAppleRegId("apple12345");

        userDao.createUser(user);
    }

    @Test
    @DatabaseSetup(
            value = "createUserExpectedDataset.xml",
            type = DatabaseOperation.CLEAN_INSERT)
    public void testGetUser() throws Exception {
        User user = userDao.getUser("test");

        assertThat(user.getId(), greaterThan(0L));
        assertThat(user.getUsername(), is("test"));
        assertThat(user.getGoogleRegId(), is("12345"));
        assertThat(user.getAppleRegId(), is("apple12345"));
        assertThat(user.getEmail(), is("test@email.com"));

        user = userDao.getUser("test2");

        assertThat(user.getId(), greaterThan(0L));
        assertThat(user.getUsername(), is("test2"));
        assertThat(user.getGoogleRegId(), is("54321"));
        assertThat(user.getEmail(), is("test2@email.com"));
    }

    @Test
    @DatabaseSetup(
            value = "createUserExpectedDataset.xml",
            type = DatabaseOperation.CLEAN_INSERT)
    public void testGetAllUser() throws Exception {

        List<User> users = userDao.getListOfUsers();

        assertEquals(users.size(), 2);
        assertThat(users.get(0).getId(), greaterThan(0L));
        assertThat(users.get(0).getUsername(), is("test"));
        assertThat(users.get(0).getGoogleRegId(), is("12345"));
        assertThat(users.get(0).getAppleRegId(), is("apple12345"));
        assertThat(users.get(0).getEmail(), is("test@email.com"));

        assertThat(users.get(1).getId(), greaterThan(0L));
        assertThat(users.get(1).getUsername(), is("test2"));
        assertThat(users.get(1).getGoogleRegId(), is("54321"));
        assertThat(users.get(1).getEmail(), is("test2@email.com"));
    }

    @Test
    @DatabaseSetup(
            value = "createUserExpectedDataset.xml",
            type = DatabaseOperation.CLEAN_INSERT)
    public void testGetUserShouldNotReturnDeletedUsers() throws Exception {
        userDao.removeUser("test");

        assertThat(userDao.getUser("test"), is(nullValue()));
    }

    @Test
    @DatabaseSetup(
            value = "createUserExpectedDataset.xml",
            type = DatabaseOperation.CLEAN_INSERT)
    public void testGetListOfUsersShouldNotReturnDeletedUsers() throws Exception {
        userDao.removeUser("test");

        assertThat(userDao.getListOfUsers().size(), is(1));
    }

    @Test
    @DatabaseSetup(
            value = "createUserSetUpDataset.xml",
            type = DatabaseOperation.DELETE_ALL)
    public void testGetUserByIdShouldNotReturnDeletedUsers() throws Exception {
        User user = new User();
        user.setUsername("test");

        userDao.createUser(user);

        userDao.removeUser("test");

        assertThat(userDao.getUserById(user.getId()), is(nullValue()));
    }
}