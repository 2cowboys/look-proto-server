package com.look.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.look.model.Photo;
import com.look.model.PhotoStatus;
import com.look.model.PendingPhoto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@ContextConfiguration(locations = {
    "/spring/applicationContext.xml"
})
@DatabaseSetup(
        value = "database_empty.xml",
        type = DatabaseOperation.DELETE_ALL)
public class PhotoDaoTest {

    @Inject
    private PhotoDao photoDao;

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT,
            value = {
                    "classpath:/com/look/dao/photos_userid12_3.xml",
                    "classpath:/com/look/dao/photos_status_userid12_4.xml"
            })
    public void testGetUserPendingPhotos() throws  Exception {

        List<Photo> pendingPhotos = photoDao.getUserPendingPhotos(12);

        assertThat(pendingPhotos.size(), is(3));

        assertThat(pendingPhotos.get(0).getId(), is(1202L));
        assertThat(pendingPhotos.get(0).getUpVotes(), is(1));
        assertThat(pendingPhotos.get(0).getDownVotes(), is(2));
        assertThat(pendingPhotos.get(0).getUpVotesSince(), is(0));
        assertThat(pendingPhotos.get(0).getDownVotesSince(), is(1));

        assertThat(pendingPhotos.get(1).getId(), is(1203L));
        assertThat(pendingPhotos.get(1).getUpVotes(), is(2));
        assertThat(pendingPhotos.get(1).getDownVotes(), is(3));
        assertThat(pendingPhotos.get(1).getUpVotesSince(), is(0));
        assertThat(pendingPhotos.get(1).getDownVotesSince(), is(1));

        assertThat(pendingPhotos.get(2).getId(), is(1201L));
        assertThat(pendingPhotos.get(2).getUpVotes(), is(0));
        assertThat(pendingPhotos.get(2).getDownVotes(), is(1));
        assertThat(pendingPhotos.get(2).getUpVotesSince(), is(0));
        assertThat(pendingPhotos.get(2).getDownVotesSince(), is(1));
    }

    @Test
    @DatabaseSetup(
            value = "tearDownAll.xml",
            type = DatabaseOperation.DELETE_ALL)
    @ExpectedDatabase(
            value = "addPhotoExpectedDataset.xml",
            table = "photos",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testAddPhoto() throws Exception {

        Photo photo = new Photo();
        photo.setUserId(11);
        photo.setPhoto(new byte[]{1, 2, 3});

        photoDao.addPhoto(photo);

        assertThat(photo.getId(), is(greaterThan(0L)));
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT, value = "classpath:/com/look/dao/photos_userid12_3.xml")
    public void testGetUserPhotos() throws Exception {

        List<Photo> photos = photoDao.getUserPhotos(12);

        assertThat(photos.size(), is(3));
        assertThat(photos.get(0).getId(), is(1202L));
        assertThat(photos.get(1).getId(), is(1203L));
        assertThat(photos.get(2).getId(), is(1201L));
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT, value = "classpath:/com/look/dao/createPendingPhotoSetUpDataset.xml")
    public void testGetUserPendingPhotosUrls() throws  Exception {

        List<Photo> pendingPhotos = photoDao.getUserPendingPhotos(12);

        assertThat(pendingPhotos.size(), is(2));

        assertThat(pendingPhotos.get(0).getId(), is(4L));
        assertThat(pendingPhotos.get(0).getUpVotes(), is(78));
        assertThat(pendingPhotos.get(0).getDownVotes(), is(8));
        assertThat(pendingPhotos.get(0).getPhotoUrl(), is("test_url2"));
        assertThat(pendingPhotos.get(0).getUpVotesSince(), is(2));
        assertThat(pendingPhotos.get(0).getDownVotesSince(), is(3));

        assertThat(pendingPhotos.get(1).getId(), is(3L));
        assertThat(pendingPhotos.get(1).getUpVotes(), is(5));
        assertThat(pendingPhotos.get(1).getDownVotes(), is(7));
        assertThat(pendingPhotos.get(1).getPhotoUrl(), is("test_url1"));
        assertThat(pendingPhotos.get(1).getUpVotesSince(), is(2));
        assertThat(pendingPhotos.get(1).getDownVotesSince(), is(3));

    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT, value = "classpath:/com/look/dao/createPendingPhotoEmptyDataSet.xml")
    @ExpectedDatabase(
            value = "classpath:/com/look/dao/addPendPhotoExpectedDataSet.xml",
            table = "photos_status",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testPendPhotoForUser() throws  Exception {

        photoDao.pendPhotoForUser(3, 12, 5);

        List<Photo> pendingPhotos = photoDao.getUserPendingPhotos(12);

        assertThat(pendingPhotos.size(), is(1));

        assertThat(pendingPhotos.get(0).getId(), is(3L));
        assertThat(pendingPhotos.get(0).getUpVotes(), is(5));
        assertThat(pendingPhotos.get(0).getDownVotes(), is(7));
        assertThat(pendingPhotos.get(0).getUpVotesSince(), is(0));
        assertThat(pendingPhotos.get(0).getDownVotesSince(), is(1));
        assertThat(pendingPhotos.get(0).getPhotoUrl(), is("test_url1"));
    }

    @Test
    @DatabaseSetup(value = "classpath:/com/look/dao/createPhotoSetUpDataset.xml",
            type = DatabaseOperation.CLEAN_INSERT)
    @ExpectedDatabase(value = "classpath:/com/look/dao/reportPhotoExpectedDataset.xml",
            table = "photos",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testReport() {
        photoDao.report(3);
        photoDao.report(3);
    }

    @Test
    @DatabaseSetup(value = "classpath:/com/look/dao/createPendingPhotoSetUpDataset.xml",
            type = DatabaseOperation.CLEAN_INSERT)
    @ExpectedDatabase(value = "classpath:/com/look/dao/updateRendingPhotoStatusExpectedDataSet.xml",
            table = "photos_status",
            assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testUpdatePendingPhotoStatus() {
        photoDao.updatePendingPhotoStatus(3, 12, PhotoStatus.LIKED);
        photoDao.updatePendingPhotoStatus(4, 12, PhotoStatus.DISLIKED);
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT,
            value = {
                    "classpath:/com/look/dao/photos_status_userid22_1.xml",
                    "classpath:/com/look/dao/photos_status_userid12_4.xml",
                    "classpath:/com/look/dao/users_id12.xml",
                    "classpath:/com/look/dao/users_id22.xml"
            })
    public void testGetPendingPhotos() {

        List<PendingPhoto> pendingPhotos = photoDao.getPendingPhotos();
        Collections.sort(pendingPhotos, new Comparator<PendingPhoto>() {
            @Override
            public int compare(PendingPhoto o1, PendingPhoto o2) {
                //CODE SMELL: bad comparator implementation but suitable in this
                //case because id values are small and will never cause overflow
                return (int) (o1.getPhotoId() - o2.getPhotoId());
            }
        });

        assertThat(pendingPhotos.size(), is(4));

        assertPendingPhoto(pendingPhotos.get(0), 1201L, 12L);
        assertPendingPhoto(pendingPhotos.get(1), 1202L, 12L);
        assertPendingPhoto(pendingPhotos.get(2), 1203L, 12L);
        assertPendingPhoto(pendingPhotos.get(3), 2201L, 22L);
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.CLEAN_INSERT,
            value = {
                    "classpath:/com/look/dao/photos_status_userid22_1.xml",
                    "classpath:/com/look/dao/photos_status_userid12_4.xml"
            })
    public void testGetPhotoStatuses() {
        String status = photoDao.getPhotoStatus(1201L, 12L);

        assertThat(status, is(PhotoStatus.UNKNOWN));

        status = photoDao.getPhotoStatus(1204L, 12L);

        assertThat(status, is(PhotoStatus.DISLIKED));
    }

    private void assertPendingPhoto(PendingPhoto pendingPhoto, long photoId, long userId) {
        assertThat(pendingPhoto.getPhotoId(), is(photoId));
        assertThat(pendingPhoto.getUserId(), is(userId));
        assertThat(pendingPhoto.getAppleRegId(), is("apple" + userId));
        assertThat(pendingPhoto.getUsername(), is("test" + userId));
    }
}